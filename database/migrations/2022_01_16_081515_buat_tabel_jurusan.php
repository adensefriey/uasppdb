<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class BuatTabelJurusan extends Migration
{

 public function up()
 {
 Schema::create('jurusan', function (Blueprint $table) {
 $table->id();
 $table->string('nama_jurusan', 100);
 $table->string('singkatan', 20);
 $table->longText('deskripsi')->nullable();
 $table->timestamps();
 });
 }
 public function down()
 {
 Schema::dropIfExists('jurusan');
 }
}