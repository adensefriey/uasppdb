<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class BuatTabelInformasi extends Migration
{

 public function up()
 {
 Schema::create('informasi', function (Blueprint $table) {
 $table->id();
 $table->string('judul', 100);
 $table->longText('konten');
 $table->enum('tampil_beranda', ['Y','N']);
 $table->timestamps();
 });
 }
 public function down()
 {
 Schema::dropIfExists('informasi');
 }
}
