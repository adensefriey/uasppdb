<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class BuatTabelSlide extends Migration
{

 public function up()
 {
 Schema::create('slide', function (Blueprint $table) {
 $table->id();
 $table->string('judul', 100);
 $table->string('deskripsi');
 $table->string('gambar');
 $table->timestamps();
 });
 }
 public function down()
 {
 Schema::dropIfExists('slide');
 }
}