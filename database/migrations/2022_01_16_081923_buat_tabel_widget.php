<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class BuatTabelWidget extends Migration
{

 public function up()
 {
 Schema::create('widget', function (Blueprint $table) {
 $table->id();
 $table->string('judul', 100);
 $table->string('posisi', 20);
 $table->text('konten');
 $table->integer('urut');
 $table->timestamps();
 });
 }
 public function down()
 {
 Schema::dropIfExists('widget');
 }
}